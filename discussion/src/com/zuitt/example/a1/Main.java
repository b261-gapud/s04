package com.zuitt.example.a1;

public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact();
        contact1.setName("Magic Gapud");
        contact1.setContactNumber("09090909090");
        contact1.setAddress("Manila, Philippines");

        Contact contact2 = new Contact();
        contact2.setName("Gladys Espedido");
        contact2.setContactNumber("09090909090");
        contact2.setAddress("Cavite, Philippines");

        phonebook.setContacs(contact1);
        phonebook.setContacs(contact2);


        if (phonebook.getContacs().isEmpty()) {
            System.out.println("No Data");
        } else {
            for (Contact contact : phonebook.getContacs()) {
                System.out.println("---------------------------------------------------------");
                System.out.println(contact.getName());
                System.out.println("---------------------------------------------------------");
                System.out.println(contact.getName() + " has a following registered number:\n" + contact.getContactNumber());
                System.out.println(contact.getName() + " has a following registered address:\n" + contact.getAddress());
            }


        }


    }
}
